import { ActionType } from "../reducers/action-types"
import { Dispatch } from "redux"
import { Action } from "../reducers/actions/index"
export const DepositMoney = (amount:number) => {
    return(dispatch:Dispatch<Action>) => {
        dispatch({

            type :ActionType.DEPOSIT,
            payload : amount
        })
    }
}
export const WithdrawMoney = (amount:number) => {
    return(dispatch:Dispatch<Action>) => {
        dispatch({

            type :ActionType.WITHDRAW,
            payload : amount
        })
    }
}

export const Bankrupt = () => {
    return(dispatch:Dispatch<Action>) => {
        dispatch({
            type :ActionType.BANKRUPT
        })
    }
}