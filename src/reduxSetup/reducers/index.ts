import { combineReducers } from "redux";
import bankReducer from '../reducers/bankReducer'
const reducer = combineReducers({

    bank : bankReducer

})

export default reducer

export type state = ReturnType <typeof reducer>