import React from 'react';
import logo from './logo.svg';
import './App.css';
import {useSelector,useDispatch} from 'react-redux'
import {state} from './reduxSetup/index'
import { bindActionCreators } from 'redux';
import {actionCreators} from './reduxSetup/index'
import Button from '@mui/material/Button';
import Box from '@mui/material/Box';

function App() {
  const title = {

    marginTop :"5em"
  }
  const dispatch = useDispatch();
  const {DepositMoney,WithdrawMoney,Bankrupt } = bindActionCreators(actionCreators,dispatch)
  const amount = useSelector((state:state)=>state.bank)
  return (
    <div className="App">
     <h1 style={title}>{amount}</h1>
     <Button variant="outlined" onClick={()=>DepositMoney(1000)}>Deposit</Button> <span> </span>  <span> </span>
     <Button variant="contained" onClick={()=>WithdrawMoney(500)}>Withdraw</Button> <span> </span>  <span> </span>
     <Button variant="outlined" onClick={()=>Bankrupt()}>Bankrupt</Button>
     {/* <button onClick={()=>DepositMoney(1000)}>Deposit</button> */}
     {/* <button onClick={()=>WithdrawMoney(500)}>Withdraw</button>
     <button onClick={()=>Bankrupt()}>Bankrupt</button> */}
  
    </div>
  );
}

export default App;
